# Flectra Community / report-print-send

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[base_report_to_printer_mail](base_report_to_printer_mail/) | 2.0.1.0.0| Report to printer - Mail extension
[remote_report_to_printer](remote_report_to_printer/) | 2.0.1.0.0| Report to printer on remotes
[server_env_printing_server](server_env_printing_server/) | 2.0.1.0.0| Server Environment for Printing Server
[base_report_to_printer](base_report_to_printer/) | 2.0.2.0.1| Report to printer
[printer_zpl2](printer_zpl2/) | 2.0.1.0.0| Add a ZPL II label printing feature


